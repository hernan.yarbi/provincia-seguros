import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    registerForm: {}
  },
  mutations: {
    setRegisterForm(state, form){
      state.registerForm = form
    }
  },
  actions: {
  },
  getters: {
    getRegisterForm(state){
      return state.registerForm
    }
  }
})
