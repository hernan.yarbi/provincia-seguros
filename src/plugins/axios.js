import axios from "axios";
import Vue from 'vue'

const axiosInstance = createInstance("http://localhost:3000");

function createInstance(baseURL){
    return axios.create({
        baseURL,
        headers: {
            'Content-Type': 'application/json',
        }
    });
}

export default {
    install () {
        Vue.prototype.$axios = axiosInstance
    }
}; 